const express = require('express');
const app = express();
const path = require('path');
const port = 3005;
const i18n = require('./i18n');


app.use(i18n);

app.use('/', express.static(path.resolve('./')));

app.set('view engine', 'ejs');

app.get('/',  (req, res) => {
    /**
     * Setting current locale to render
     */
    res.setLocale('en');

    res.render('index');
});

app.get('/fr/',  (req, res) => {
    /**
     * Setting current locale to render
     */
    res.setLocale('fr');

    res.render('index');
});


app.listen(port,  () => {
    console.log(`App listening on port ${port}!`);
});

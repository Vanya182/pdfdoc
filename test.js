var fs = require('fs');
var pdf = require('html-pdf');
var html = fs.readFileSync('./index.html', 'utf8');
var options = { 
	// "format": 'A3',
	// "orientation": "portrait",
	"height": "1024px",
	"width": "992px",
    "base": "file:///Users/vanya/Projects/247labs/mdp/invoice/",
	"border": {
	    "top": "1cm",
	    "right": "1cm",
	    "bottom": "1cm",
	    "left": "1cm"
	  },
  	"zoomFactor": "1",
  	"type": "pdf",
  	"renderDelay": 3000
};
 
pdf.create(html, options).toFile('./test.pdf', function(err, res) {
  if (err) return console.log(err);
  console.log(res);
});
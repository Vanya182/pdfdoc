const
    webpack = require('webpack'),
    path = require('path'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),

    webpackConfig = {
        context: __dirname,
        entry: {
            styles: './css/index.scss'
        },
        output: {
            path: __dirname + '/build'
        },
        resolve: {
            modules: [path.resolve(__dirname, "app"), "node_modules"],
            descriptionFiles: ["package.json"],
            extensions: [".css"]
        },
        devtool: '#cheap-module-source-map',
        devServer: {
            inline: true,
            port: 3333,
            stats: {colors: true}
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: ["css-loader", "sass-loader"]
                    })
                },
                {
                    test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
                    use: 'file-loader'
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin({ filename: 'styles.css', allChunks: true }),
        ],
        watch: true
    };

module.exports = webpackConfig;